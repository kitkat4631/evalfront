var content = document.querySelector('#burger-content');
var sidebarBody = document.querySelector('#burger-sidebar-body');
var button = document.querySelector('#burger-button');
var overlay = document.querySelector('#burger-overlay');
var activatedClass = 'burger-activated';

sidebarBody.innerHTML = content.innerHTML;

button.addEventListener('click', function(e) {
    e.preventDefault();

    this.parentNode.classList.add(activatedClass);
});

button.addEventListener('keydown', function(e) {
    if (this.parentNode.classList.contains(activatedClass))
    {
        if (e.repeat === false && e.which === 27)
           this.parentNode.classList.remove(activatedClass);
    }

});

overlay.addEventListener('click', function(e) {
    e.preventDefault();
    this.parentNode.classList.remove(activatedClass);
});